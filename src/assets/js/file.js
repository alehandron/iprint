$(document).ready(function(){
    $('.slider__inner').slick({
        dots: false,
        variableWidth:true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        prevArrow: $('.btn--prev'),
        nextArrow: $('.btn--next')
    });

    $('works__tab').slick({
        dots: false,
        variableWidth:true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
    });
});
