function openWorks(evt, worksName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("works__images");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.opacity = "0";
    }
    tablinks = document.getElementsByClassName("btn--tab");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(worksName).style.opacity = "1";
    evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
